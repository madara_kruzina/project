//Print x integers starting from y descending//

import java.util.Scanner;

public class Exercise2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        System.out.println("Enter count value: ");
        int x = scanner.nextInt();


        System.out.println("Enter starting value: ");
        int y = scanner.nextInt();

        forLoop(x, y);
        whileLoop(x, y);
        doWhileLoop(x, y);}

    private static void whileLoop(int x, int y) {
        System.out.println("While loop /decending/: ");
        while (--x >= 0) {
            System.out.println(y--);
        }

    }

    private static void doWhileLoop(int x, int y) {
        System.out.println("Do-while loop /decending/: ");
        int limit = y - x;
        do {
            System.out.println(y--);
        }
        while (y > limit);

    }

    private static void forLoop(int x, int y) {
        System.out.println("For loop /decending/: ");
        for (int i = 0; i < x; i++) {
            System.out.println(y--);
        }


    }

}


